import React from 'react';
import { FaFacebookF, FaTwitter, FaInstagram } from 'react-icons/fa';
import '../Footer/Footer.css';

function Footer() {
  return (
    <footer>
      <div className='footer-container'>
        <div className='footer-contact'>
          <h3>Contact Us</h3>
          <p>Email: info@example.com</p>
          <p>Phone: +1234567890</p>
          <p>Address: 123 Street, City, Country</p>
        </div>
        <div className='social-media'>
          <h3>Follow Us</h3>
          <ul>
            <li>
              <a href='/'>
                <FaFacebookF /> Facebook
              </a>
            </li>
            <li>
              <a href='/'>
                <FaTwitter /> Twitter
              </a>
            </li>
            <li>
              <a href='/'>
                <FaInstagram /> Instagram
              </a>
            </li>
          </ul>
        </div>

        <div className='legal'>
          <h3>Legal</h3>
          <ul>
            <li>
              <a href='/'>Privacy Policy</a>
            </li>
            <li>
              <a href='/'>Terms of Service</a>
            </li>
          </ul>
        </div>
      </div>
      <div className='bottom-footer'>
        <p>&copy; 2024 Your NGO Name. All rights reserved.</p>
        <p>Designed by Your Company Name</p>
      </div>
    </footer>
  );
}

export default Footer;
