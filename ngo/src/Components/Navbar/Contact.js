import React from 'react';
import './Contact.css'; // Import your CSS file for Contact styles

function Contact() {
  return (
    <section className='contact'>
      <div className='contact-container'>
        <h2>Contact Us</h2>
        <p>
          If you have any questions or inquiries, feel free to contact us using
          the form below:
        </p>
        <form action='#' method='post'>
          <div className='form-group'>
            <label htmlFor='name'>Name</label>
            <input type='text' id='name' name='name' required />
          </div>
          <div className='form-group'>
            <label htmlFor='email'>Email</label>
            <input type='email' id='email' name='email' required />
          </div>
          <div className='form-group'>
            <label htmlFor='message'>Message</label>
            <textarea id='message' name='message' rows='5' required></textarea>
          </div>
          <button type='submit' className='btn'>
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
}

export default Contact;
