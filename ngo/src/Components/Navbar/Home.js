import React from 'react';
import '../Navbar/Home.css';
import helpingHandsImage from '../../Assets/images/helpinghands.jpg';
import poorChildrenImage from '../../Assets/images/poorchildren.jpg';
import educationChildrenImage from '../../Assets/images/educationchildren.jpg';
import fundraisingImage from '../../Assets/images/fundraising.jpg';
import awarenessCampaignsImage from '../../Assets/images/AwarenessCampaigns.jpg';
import volunteeringOpportunitiesImage from '../../Assets/images/VolunteeringOpportunities.jpg';
import summerGalaImage from '../../Assets/images/gala.jpg';
import cleanUpDayImage from '../../Assets/images/cleaupday.jpg';
import accImage from '../../Assets/images/acc.png';
import settingImage from '../../Assets/images/setting.png';
import addEventImage from '../../Assets/images/add-event.png';
import channelsImage from '../../Assets/images/channels.png';
import abcCEOImage from '../../Assets/images/ABCceo.jpg';
import xyzCEOImage from '../../Assets/images/XYZceo.png';

function Home() {
  return (
    <div>
      <section className='hero'>
        <div className='container'>
          <div className='hero-content'>
            <div className='slider'>
              <div className='slide'>
                <img src={helpingHandsImage} alt='Helping Hands' />
                <div className='description'>
                  Helping Hands: Empowering communities through collective
                  action
                </div>
              </div>
              <div className='slide'>
                <img src={poorChildrenImage} alt='Poor Children' />
                <div className='description'>
                  "Poor Children: Empowering disadvantaged youth for a brighter
                  tomorrow."
                </div>
              </div>
              <div className='slide'>
                <img src={educationChildrenImage} alt='Education Children' />
                <div className='description'>
                  "Education Children: Empowering young minds through
                  education."
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* event section */}
      <section className='event-categories'>
        <div className='container'>
          <h2>Event Categories</h2>
          <div className='category-grid'>
            <div className='category-card'>
              <img src={fundraisingImage} alt='Fundraising' />
              <h3>Fundraising</h3>
              <p>Raise funds for a cause through various events.</p>
            </div>
            <div className='category-card'>
              <img src={awarenessCampaignsImage} alt='Awareness Campaigns' />
              <h3>Awareness Campaigns</h3>
              <p>Create awareness about important social issues.</p>
            </div>
            <div className='category-card'>
              <img
                src={volunteeringOpportunitiesImage}
                alt='Volunteering Opportunities'
              />
              <h3>Volunteering Opportunities</h3>
              <p>Get involved and make a difference through volunteering.</p>
            </div>
          </div>
        </div>
      </section>
      {/* feature events */}
      <section className='featured-events'>
        <div className='container'>
          <h2>Featured Events</h2>
          <div className='event-cards'>
            <div className='event-card'>
              <div className='event-image'>
                <img src={summerGalaImage} alt='Event 1' />
              </div>
              <div className='event-details'>
                <h3>Summer Charity Gala</h3>
                <p>Date: August 15, 2025</p>
                <p>Location: City Convention Center, New York</p>
                <p>
                  Description: Join us for a glamorous evening of charity,
                  auctions, and entertainment to support underprivileged
                  children.
                </p>
                <button className='btn'>View Details</button>
              </div>
            </div>
            <div className='event-card'>
              <div className='event-image'>
                <img src={cleanUpDayImage} alt='Event 2' />
              </div>
              <div className='event-details'>
                <h3>Community Clean-Up Day</h3>
                <p>Date: September 20, 2025</p>
                <p>Location: City Park, San Francisco</p>
                <p>
                  Description: Volunteer with us to clean up our local park and
                  contribute to a cleaner, greener environment.
                </p>
                <button className='btn'>View Details</button>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* how it works */}
      <section className='how-it-works'>
        <div className='container'>
          <h2>How It Works</h2>
          <div className='steps'>
            <div className='step'>
              <img src={accImage} alt='Step 1' />
              <p>Create an account on our platform.</p>
            </div>
            <div className='step'>
              <img src={settingImage} alt='Step 2' />
              <p>Set up your NGO profile.</p>
            </div>
            <div className='step'>
              <img src={addEventImage} alt='Step 3' />
              <p>Create your event with all the necessary details.</p>
            </div>
            <div className='step'>
              <img src={channelsImage} alt='Step 4' />
              <p>Promote your event through various channels.</p>
            </div>
          </div>
        </div>
      </section>
      {/* testimonials */}
      <section className='testimonials'>
        <div className='container'>
          <h2>Testimonials</h2>
          <div className='testimonial'>
            <img src={abcCEOImage} alt='Participant 1' />
            <blockquote>
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
              volutpat felis eu felis aliquet, a volutpat ante malesuada."
              <cite>- John Doe, CEO of XYZ Foundation</cite>
            </blockquote>
          </div>
          <div className='testimonial'>
            <img src={xyzCEOImage} alt='Participant 2' />
            <blockquote>
              "Vivamus a felis nec tortor fermentum elementum eget ut tellus.
              Nulla facilisi."
              <cite>- Jane Smith, Founder of ABC Charity</cite>
            </blockquote>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Home;
