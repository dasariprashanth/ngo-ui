import React from 'react';
import '../Navbar/AboutUs.css';

function About() {
  return (
    <div>
      <section class='about-us'>
        <div class='about-container'>
          <h2>About Us</h2>
          <p>
            Welcome to NGO Event Platform, where we strive to empower NGOs to
            create meaningful events for positive change. Our mission is to
            provide a user-friendly platform that facilitates event creation,
            management, and promotion, allowing NGOs to focus on their core
            mission of making a difference in the world.
          </p>
          <p>
            At NGO Event Platform, we believe in the power of community and
            collaboration. Our dedicated team is committed to supporting NGOs in
            their efforts to raise awareness, funds, and engagement for
            important social causes.
          </p>
          <p>
            Join us in our journey to create a better world, one event at a
            time.
          </p>
        </div>
      </section>
    </div>
  );
}

export default About;
