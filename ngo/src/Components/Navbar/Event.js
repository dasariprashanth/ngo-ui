import React, { useState } from 'react';
import './Event.css'; // Import the CSS file for styling

function Event() {
  // State to manage form data for creating an event
  const [formData, setFormData] = useState({
    title: '',
    date: '',
    time: '',
    location: '',
    description: '',
    goals: '',
    audience: '',
    beneficiary: '',
    image: null,
    // New state for event customization
    colorScheme: '',
    font: '',
    branding: '',
  });

  // Function to handle form input changes
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  // Function to handle image upload
  const handleImageChange = (e) => {
    const image = e.target.files[0];
    setFormData({ ...formData, image });
  };

  // Function to handle form submission
  const handleSubmit = (e) => {
    e.preventDefault();
    // Submit form data to backend or perform further actions
    console.log(formData);
  };

  // Function to handle event customization changes
  const handleCustomizationChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  // Function to preview event page
  const handlePreview = () => {
    // Navigate to a preview page or display a modal with the preview
    console.log('Preview event page');
  };

  return (
    <div className='event-page'>
      <section className='event-form'>
        <div className='event-form-container'>
          <h2>Create Event</h2>
          <form onSubmit={handleSubmit}>
            <div className='form-section'>
              <input
                type='text'
                name='title'
                placeholder='Event Title'
                value={formData.title}
                onChange={handleChange}
                required
              />
              <input
                type='date'
                name='date'
                value={formData.date}
                onChange={handleChange}
                required
              />
              <input
                type='time'
                name='time'
                value={formData.time}
                onChange={handleChange}
                required
              />
              <input
                type='text'
                name='location'
                placeholder='Location'
                value={formData.location}
                onChange={handleChange}
                required
              />
              <textarea
                name='description'
                placeholder='Event Description'
                value={formData.description}
                onChange={handleChange}
                required
              />
              <input
                type='file'
                accept='image/*'
                name='image'
                onChange={handleImageChange}
              />
            </div>
            <div className='form-section'>
              <textarea
                name='goals'
                placeholder='Event Goals'
                value={formData.goals}
                onChange={handleChange}
                required
              />
            </div>
            <div className='form-section'>
              <textarea
                name='audience'
                placeholder='Target Audience'
                value={formData.audience}
                onChange={handleChange}
                required
              />
            </div>
            <div className='form-section'>
              <textarea
                name='beneficiary'
                placeholder='Beneficiary Information'
                value={formData.beneficiary}
                onChange={handleChange}
                required
              />
            </div>
            <div className='form-section'>
              <h3>Event Customization</h3>
              <input
                type='color'
                name='colorScheme'
                value={formData.colorScheme}
                onChange={handleCustomizationChange}
              />
              <input
                type='text'
                name='font'
                placeholder='Font (e.g., Arial, Helvetica)'
                value={formData.font}
                onChange={handleCustomizationChange}
              />
              <input
                type='text'
                name='branding'
                placeholder='Branding (e.g., Logo URL)'
                value={formData.branding}
                onChange={handleCustomizationChange}
              />
              <button type='button' className='btn' onClick={handlePreview}>
                Preview Event Page
              </button>
            </div>
            <button type='submit' className='btn'>
              Create Event
            </button>
          </form>
        </div>
      </section>
    </div>
  );
}

export default Event;
