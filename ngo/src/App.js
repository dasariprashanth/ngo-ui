import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './Components/Navbar/Home';
import About from './Components/Navbar/AboutUs';
import Contact from './Components/Navbar/Contact';
import Navbar from './Components/Navbar/Navbar';
import Footer from './Components/Footer/Footer';
import Event from './Components/Navbar/Event';
function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        {' '}
        <Route path='/' element={<Home />} />{' '}
        <Route path='/about' element={<About />} />
        <Route path='/contact' element={<Contact />} />
        <Route path='/event' element={<Event />} />
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
